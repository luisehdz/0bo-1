﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class killzone : MonoBehaviour
{
    public string levelToLoad;
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger != true)
        {
            if (collision.CompareTag("Player"))
            {
                //collision.GetComponent<PlayerMovement>().takeDamage(5);
                SceneManager.LoadScene(levelToLoad);
            }
        }
    }
}
