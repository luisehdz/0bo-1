﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator anim;
    public GameObject firepoint;
    public GameObject shield;
    public HUD screen;

    public int health = 100;
    public float speed = 0.2f;
    public float jumpPower = 400f;
    float x;

    public bool isGrounded = false;
    public bool canDoubleJump = false;
    public bool isDead = false;
    public bool hasPUDoubleJump = false;
    public bool hasPUShield = false;
    public bool hasPUEnchancedLaser = false;

    public bool win = false;

    private bool isFacingRight = true;

    public GameObject laser;
    public AudioSource deathSfx;
    public AudioSource damageSfx;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        screen = FindObjectOfType<HUD>();

        shield.SetActive(false);
    }

    void Update()
    {
        if(!isDead)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                anim.SetTrigger("usedBeam");
            }

            //Makes Player Jump
            if (Input.GetKeyDown("space"))
            {
                if (isGrounded && Time.deltaTime != 0f)
                {
                    rb.AddForce(Vector2.up * jumpPower);
                }
                else
                {
                    //Checks if the player is able to double jump
                    if (canDoubleJump)
                    {
                        canDoubleJump = false;
                        rb.velocity = new Vector2(rb.velocity.x, 0);
                        rb.AddForce(Vector2.up * (jumpPower - 50));
                    }
                }
            }

            if (x != 0)
            {
                anim.SetBool("isRunning", true);
            }
            else
            {
                anim.SetBool("isRunning", false);
            }

            if (Input.GetAxis("Horizontal") > 0f && isFacingRight)
            {
                var tarScale = transform.localScale;
                tarScale.x = -Mathf.Abs(tarScale.x);
                transform.localScale = tarScale;
                firepoint.transform.Rotate(0f, 0f, 0f);
                isFacingRight = !isFacingRight;

            }
            else if (Input.GetAxis("Horizontal") < 0f && !isFacingRight)
            {
                var tarScale = transform.localScale;
                tarScale.x = Mathf.Abs(tarScale.x);
                transform.localScale = tarScale;
                firepoint.transform.Rotate(0f, 0f, 0f);
                isFacingRight = !isFacingRight;
            }
        }
        
        if(hasPUShield)
        {
            shield.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        if(!isDead)
        {
            //Makes the player move left or right depending on user input
            x = Input.GetAxis("Horizontal");
            transform.Translate((Vector3.right * speed) * x);
        }
    }

    public void takeDamage(int damage)
    {
        damageSfx.Play();

        if(hasPUShield)
        {
            shield.SetActive(false);
            hasPUShield = false;
        }
        else
        {
            health -= damage;
            if (health < 0)
            {
                deathSfx.Play();
                StartCoroutine(resetLevel());
                health = 0;
                isDead = true;
                anim.SetBool("isDead", true);
            }
        }
    }

    IEnumerator resetLevel()
    {
        yield return new WaitForSeconds(3f);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Finish"))
        {
            screen.endLevel = true;
        }
    }
}
