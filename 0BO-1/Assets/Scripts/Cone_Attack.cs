﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cone_Attack : MonoBehaviour
{
    public TurretAI turretAI;

    public bool isLeft = false;

    // Start is called before the first frame update
    void Awake()
    {
        turretAI = gameObject.GetComponentInParent<TurretAI>();
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            if(isLeft)
            {
                turretAI.Attack(false);
            }
            else
            {
                turretAI.Attack(true);
            }
        }
    }
}
