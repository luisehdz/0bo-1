﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam : MonoBehaviour
{
    bool laser = false;
    bool switchBeams = true;
    public float force = 6000f;

    void Start()
    {
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            laser = true;
        }
        else if (Input.GetKeyUp(KeyCode.Q))
        {
            switchBeams = !switchBeams;
        }
    }

    //Depending on whether the laser beam is set to pull or push this will act accordingly
    private void OnTriggerStay2D(Collider2D hit)
    {
        Rigidbody2D entityRB = hit.GetComponent<Rigidbody2D>();

        if (hit.CompareTag("IntObj") && laser && switchBeams)
        {
            entityRB.AddForce(new Vector2(force, 0));
        }
        else if (hit.CompareTag("IntObj") && laser && !switchBeams)
        {
            entityRB.AddForce(new Vector2(-force, 0));
        }

        laser = false;
    }
}
