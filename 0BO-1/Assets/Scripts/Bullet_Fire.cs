﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Fire : MonoBehaviour
{
    public int damage = 30;

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("hit!");

        if (collision.collider == true)
        {
            Debug.Log("ho");
            if (collision.collider.CompareTag("Player"))
            {
                Debug.Log("smack");
                collision.collider.GetComponent<PlayerMovement>().takeDamage(damage);
            }
        }

        Destroy(gameObject);
    }
}
