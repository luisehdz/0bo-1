﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum powerup
{
    None,
    DoubleJump,
    Shield,
    EnchancedLaser
}

public class Pickup : MonoBehaviour
{
    public powerup PU;
    public int speed;
    public float amplitude;
    private float tempVal;
    private Vector3 posY;
    private PlayerMovement player;

    public AudioSource pickup;

    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
        tempVal= transform.position.y;
    }

    void Update()
    {
        //posY.y = tempVal + amplitude * Mathf.Sin(speed * Time.time);
        //transform.position = posY;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        pickup.Play();

        if (collision.gameObject.CompareTag("Player"))
        {
            if(PU == powerup.DoubleJump)
            {
                player.hasPUDoubleJump = true;
            }
            else if(PU == powerup.Shield)
            {
                player.hasPUShield = true;
            }
            else if(PU == powerup.EnchancedLaser)
            {
                player.hasPUEnchancedLaser = true;
            }

            Destroy(gameObject);
        }
    }
}
