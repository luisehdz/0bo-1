﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    private PlayerMovement player;

    void Start()
    {
        player = gameObject.GetComponentInParent<PlayerMovement>();
    }

    //checks if the Player is on a ground
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.isTrigger)
        {
            player.isGrounded = true;
        }

    }
    void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.isTrigger)
        {
            player.isGrounded = true;
        }
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.isTrigger)
        {
            player.isGrounded = false;
            if(player.hasPUDoubleJump == true)
            {
                player.canDoubleJump = true;
            }
        }
    }
}
