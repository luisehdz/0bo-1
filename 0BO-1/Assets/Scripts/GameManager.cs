﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private static bool created = false;

    static public GameManager Get()
    {
        return instance;
    }

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public AudioManager audioManager;

    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        { 
            Debug.LogError("No audio manger found in scene");
        }

        audioManager.PlaySound("BGMusic");
    }

    public void changeTrack(string name)
    {
        audioManager.PlaySound(name);
    }

}
