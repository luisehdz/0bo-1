﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAI : MonoBehaviour
{
    public float distance;
    public float wakeRange;
    public float shootInterval;
    public float bulletSpeed = 100;
    public float bulletCooldown;

    public bool awake = false;
    public bool lookingRight = true;
    bool destroyed = false;

    private PlayerMovement player;
    public GameObject bullet;
    public Animator anim;
    public Transform shootingRight, shootingLeft;
    BoxCollider2D rb;
    public AudioSource shootingSfx;

    Vector3 offsetDist = new Vector3(2, 0, 0);

    void Awake()
    {
        anim = gameObject.GetComponent<Animator>(); 
        player = FindObjectOfType<PlayerMovement>();
        rb = GetComponent<BoxCollider2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(destroyed)
        {
            Debug.Log("Ouch");
            anim.SetTrigger("dead");
            Destroy(rb);
            this.enabled = false;
        }
        else
        {
            anim.SetBool("Awake", awake);

            RangeCheck();
            if (player.transform.position.x > transform.position.x)
            {
                lookingRight = true;
            }
            else
            {
                lookingRight = false;
            }
        }
    }

    void RangeCheck()
    {
        distance = Vector3.Distance(transform.position, player.transform.position);

        if (distance < wakeRange)
        {
            awake = true;
        }
        else
        {
            awake = false;
            bulletCooldown = 0;
        }
    }

    public void Attack(bool attackingRight)
    {
        if(!player.isDead && !destroyed)
        {
            bulletCooldown += Time.deltaTime;

            if (bulletCooldown >= shootInterval)
            {
                shootingSfx.Play();
                Vector2 direction = player.transform.position - transform.position;
                direction.Normalize();

                if (!lookingRight)
                {
                    GameObject bulletClone;
                    bulletClone = Instantiate(bullet, shootingLeft.transform.position - offsetDist, shootingLeft.transform.rotation) as GameObject;
                    bulletClone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

                    bulletCooldown = 0;
                }
                else
                {
                    GameObject bulletClone;
                    bulletClone = Instantiate(bullet, shootingRight.transform.position + offsetDist, shootingRight.transform.rotation) as GameObject;
                    bulletClone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

                    bulletCooldown = 0;
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("IntObj"))
        {
            destroyed = true;
        }
    }
}
