﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject endScreen;

    public Slider DoubleJump;
    public Image Shield;
    private bool isPaused = false;
    public bool endLevel = false;
    private PlayerMovement player;

    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
        pauseMenu = GameObject.Find("Canvas/PauseMenu");
        pauseMenu.SetActive(false);
        endScreen.SetActive(false);

        Shield.enabled = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        //GameManager.Get().audioManager.PlaySound("LevelMusic");
    }

    void Update()
    {
        //This either pauses the game or unpauses the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                onResumeButton();
            }
            else
            {
                onPause();
            }
        }

        if(player.hasPUShield)
        {
            Shield.enabled = true;
        }
        else
        {
            Shield.enabled = false;
        }

        DoubleJump.value = player.health;

        if(endLevel)
        {
            endScreen.SetActive(true);
            StartCoroutine("Reset");
        }
    }

    IEnumerator Reset()
    {
        yield return new WaitForSeconds(2f);
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("Title");
    }

    public void onResumeButton()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        isPaused = false;
        Time.timeScale = 1f;

        pauseMenu.SetActive(false);
    }

    public void onResetButton()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        isPaused = false;
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);

        SceneManager.LoadScene("BlackBox");
    }

    public void onPause()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        isPaused = true;
        Time.timeScale = 0f;

        pauseMenu.SetActive(true);
    }

    public void onQuitButton()
    {
        Application.Quit();
    }
}