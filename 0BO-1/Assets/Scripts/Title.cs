﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    public string levelToLoad;
    public AudioSource buttonSfx;

    public void onPlay()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    public void onQuit()
    {
        Application.Quit();
    }
}
